<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\UserInfo;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       User::factory(20)->create();
       $userinfo = UserInfo::create([
           'first_name' => 'Princess',
           'last_name' => 'Batestil',
           'gender' => 'Female',
           'address' => 'M.H. Del Pilar St, Downtown, Tacloban City, Leyte',
           'birthday' => '1999-03-01',
           'contact_number' => '09123456789',
           'role_id' => 1,
       ]);

       User::create([
           'email' => 'admin@admin.com',
           'password' => Hash::make('123123'),
           'user_info_id' => $userinfo->id,
       ]);
    }
}
