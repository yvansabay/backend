<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RentalInfo extends Model
{
    use HasFactory, SoftDeletes;
    protected $guarded = [];

    public function driver(){
        return $this->belongsTo(Driver::class, 'driver_id', 'id')->withTrashed();
    }

}
