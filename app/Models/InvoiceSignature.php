<?php

use LaravelDaily\Invoices\Invoice;

/**
 * Class InvoiceSignature
 * 
 */
class InvoiceSignature extends Invoice
{

   /*
   * @var string
   */
   public $signature;

     /**
     * InvoiceSignature constructor.
     */
    public function __construct()
    {
    }

   /**
     * @param string $signature
     * @return $this
     */
   public function signature(string $signature)
   {
      $this->signature = $signature;
      return $this;
   }


   public function getSignature()
   {
       $type = pathinfo($this->signature, PATHINFO_EXTENSION);
       $data = file_get_contents($this->signature);

       return 'data:image/' . $type . ';base64,' . base64_encode($data);
   }

}