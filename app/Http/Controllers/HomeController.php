<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\Car;
use App\Models\CarBrand;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', 'filteredCars']]);
    }

    public function index(){

        $branch = Branch::latest()->get(['id', 'branch']);
        $cars = Car::where('for_rent_status', 'Approved')->with(['rate', 'branch', 'brand', 'user.info'])->latest()->take(15)->get();
        $brands = CarBrand::latest()->get(['id', 'brand']);

        $data = array();
        $data['id'] = 0;
        $data['branch'] = 'All Branch';

        $branch->prepend($data, 0);
        return response()->json([
            'msg' => 'Data retrieved successfully!',
            'branch' => $branch,
            'cars' => $cars,
            'brands' => $brands,
        ]);
    }

    public function filteredCars(Request $request){
        
        if($request->pickup_date == ''){
            $cars = Car::where('for_rent_status', 'Approved')->with(['rate', 'branch', 'brand', 'user.info'])->latest()->get();
            return $this->success('Cars data has been retrieved successfully!', $cars);

        }
        if($request->branch_id == 0){
            $cars = Car::where('for_rent_status', 'Approved')->whereDoesntHave('rental.rental_info', function($query) {
                $query->whereBetween('pickup_date', [request('pickup_date'), request('return_date')])
                ->orWhereBetween('return_date', [request('pickup_date'), request('return_date')]);
            })->with(['rental', 'rental.rental_info', 'rate', 'branch', 'brand', 'user.info'])->latest()->get();
        }
        else {
            $cars = Car::where('for_rent_status', 'Approved')->where('branch_id', $request->branch_id)->whereDoesntHave('rental.rental_info', function($query) {
                $query->whereBetween('pickup_date', [request('pickup_date'), request('return_date')])
                ->orWhereBetween('return_date', [request('pickup_date'), request('return_date')]);
            })->with(['rental', 'rental.rental_info', 'rate', 'branch', 'brand', 'user.info'])->latest()->get();
        }

        return $this->success('Cars data has been retrieved successfully!', $cars);
    }
}
