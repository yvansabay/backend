<?php

namespace App\Http\Controllers;

use App\Models\Driver;
use App\Models\Rental;
use Illuminate\Http\Request;

class DriverController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(){
        $drivers = Driver::latest()->get();
        return $this->success('Drivers has been retrieved successfully!', $drivers);
    }

    public function filteredDrivers(Request $request){

        $rental = Rental::where('id', $request->id)->with(['rental_info','car','car.brand'])->first();
        if($rental){
            $drivers = Driver::whereDoesntHave('rental_info', fn($query) => 
                $query->whereBetween('pickup_date', [$rental->rental_info->pickup_date, $rental->rental_info->return_date])
                ->orWhereBetween('return_date', [$rental->rental_info->pickup_date, $rental->rental_info->return_date])
            )->latest()->get(['id','first_name','last_name','email','gender']);

            $drivers->map(function($item, $key) {
                $item['full_name'] = $item['first_name'] . ' ' . $item['last_name'] . ' - ' . $item['gender'];
                return $item;
            });

            return $this->success('Available drivers has been retrieved successfully!', $drivers);
        }
        else {
            return $this->error('Invalid rental id has been passed');
        }
    }

    public function store(Request $request) {
        $driver = Driver::create([
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'birthday' => $request->birthday,
            'contact_number' => $request->contact_number,
            'gender' => $request->gender,
            'address' => $request->address,
            'email' => $request->email,
            'profile_img' => $request->profile_img_uploaded,
            'drivers_license' => $request->drivers_license,
        ]);

        return $this->success('Driver has been added successfully!', $driver);
    }

    public function update(Request $request, $id) {
        $drivers = Driver::where('id', $id)->first();
        $drivers->update([
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'birthday' => $request->birthday,
            'contact_number' => $request->contact_number,
            'gender' => $request->gender,
            'address' => $request->address,
            'profile_img' => $request->profile_img_uploaded,
            'drivers_license' => $request->drivers_license,
        ]);

        return $this->success('Driver has been updated successfully!', $drivers);
    }

    public function destroy($id){
        Driver::destroy($id);
        $drivers = Driver::onlyTrashed()->where('id', $id)->first();
        return $this->success('Driver has been archived', $drivers);
    }
}
