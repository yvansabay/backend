<?php

namespace App\Http\Controllers;

use App\Models\Car;
use App\Models\Payment;
use App\Models\Rental;
use App\Models\RentalInfo;
use Illuminate\Http\Request;
use LaravelDaily\Invoices\Invoice;
use LaravelDaily\Invoices\Classes\Buyer;
use LaravelDaily\Invoices\Classes\InvoiceItem;
use App\Models\InvoiceSignature;
use InvoiceSignature as GlobalInvoiceSignature;
use LaravelDaily\Invoices\Classes\Party;

class PaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function store(Request $request){
        $payment = Payment::create([
            'invoice' => $this->invoice($request),
            'rental_id' => $request->id,
            'total_payment' => $request->rental_info['total_payment'],
            'change' => $request->amount_tendered - $request->rental_info['total_payment'],
            'amount_tendered' => $request->amount_tendered
        ]);

        $rental = Rental::where('id', $request->id)->first();
        $rental->load(['rental_info']);

        $rental->rental_info->update([
            'driver_id' => $request->driver_id,
            'payment_status' => 'Paid'
        ]);
        
        $car = Car::where('id', $rental->car_id)->first();
        $rental->update([
            'status' => 'On-going'
        ]);
        $car->update([
            'rental_status' => 'On-going'
        ]);
        
        return $this->success('Payment successful. The user may now get the car', $payment);
    }

    public function invoice($data){
        $car = Car::with(['brand','user','user.info'])->where('id', $data->car_id)->first();
        $rental = Rental::where('id', $data->id)->first();
        $rental->load(['rental_info', 'car', 'user.info']);
        $client = new Party([
            'name'  => $car->user->info->last_name . ', ' . $car->user->info->first_name . ' ' . $car->user->info->middle_name,
            'phone' => $car->user->info->contact_number,
            'custom_fields' => [
                'email' => $car->user->email,
                'address' => $car->user->info->address,
            ],
        ]);
        
        $customer = new Buyer([
            'name'          => auth()->user()->info->last_name . ', ' .auth()->user()->info->first_name . ' ' . auth()->user()->info->middle_name,
            'custom_fields' => [
                'email' => auth()->user()->email,
                'contact' => auth()->user()->info->contact_number,
                'address' => auth()->user()->info->address
            ],
        ]);

        $itemPayment = (new InvoiceItem())
        ->title('Total Payment')
        ->quantity($rental->rental_info->total_days)
        ->subTotalPrice($rental->rental_info->total_payment)
        ->pricePerUnit($rental->rental_info->total_payment);

        $itemAmount = (new InvoiceItem())
        ->title('Amount Tendered')
        ->quantity(1)
        ->subTotalPrice($data->amount_tendered)
        ->pricePerUnit($data->amount_tendered);

        $itemChange = (new InvoiceItem())
        ->title('Change')
        ->quantity(1)
        ->pricePerUnit($data->amount_tendered - $data->rental_info['total_payment']);
  
        $invoice = Invoice::make()
            ->buyer($customer)
            ->seller($client)
            ->currencySymbol('₱')
            ->currencyCode('PHP')
            ->template('invoice-paid')
            ->status(__('invoices::invoice.paid'))
            ->currencyFormat('{SYMBOL}{VALUE}')
            ->addItem($itemAmount)
            ->addItem($itemPayment)
            ->addItem($itemChange)
            ->logo(public_path('logo/logo.png'))
            ->filename('paid-invoice-'.time())
            ->totalAmount($rental->rental_info->total_payment)
            ->save('invoice');


        $link = $invoice->url();
        return $link;
    }
}
