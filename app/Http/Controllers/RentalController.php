<?php

namespace App\Http\Controllers;

use App\Models\Car;
use App\Models\Payment;
use App\Models\Rental;
use App\Models\RentalInfo;
use App\Models\User;
use Exception;
use LaravelDaily\Invoices\Classes\Party;
use Illuminate\Http\Request;
use LaravelDaily\Invoices\Invoice;
use LaravelDaily\Invoices\Classes\Buyer;
use LaravelDaily\Invoices\Classes\InvoiceItem;
use Stripe;

class RentalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(){
        if(auth()->user()->info->role->role == 'Admin'){
            $rental = Rental::with(['rental_info.driver','car:id,car_rate_id,rental_status,car_brand_id,model,plate_number,year,seats,vehicle_identification_number', 'car.brand:id,brand,logo', 'rental_info', 'user.info', 'payment', 'car.rate'])->latest()->get();
            $rental->map(function($rental, $key) {
                if($rental->rental_info['driver']){
                    $rental->rental_info['driver']['full_name'] = $rental->rental_info['driver']['first_name'] . ' ' . $rental->rental_info['driver']['last_name'];
                }
                return $rental;
            });
            return $this->success('Rental data has been retrieved successfully!', $rental);
        }
        else {
            $rental = Rental::where('rentee_id', auth()->user()->id)->orWhereRelation('car', 'user_id', auth()->user()->id)->with(['rental_info.driver','car:id,car_rate_id,    rental_status,car_brand_id,model,plate_number,year,seats,vehicle_identification_number', 'car.brand:id,brand,logo', 'rental_info', 'user.info', 'payment', 'car.rate'])->latest()->get();
            $rental->map(function($rental, $key) {
                if($rental->rental_info['driver']){
                    $rental->rental_info['driver']['full_name'] = $rental->rental_info['driver']['first_name'] . ' ' . $rental->rental_info['driver']['last_name'];
                }
                return $rental;
            });
            return $this->success('Rental data has been retrieved successfully!', $rental);
        }
    }

    public function markFinished(Request $request){
        $rental = Rental::where('id', $request->id)->first();
        $rental->update([
            'status' => 'Finished'
        ]);

        $rental->load(['car']);

        $rental->car->update([
            'rental_status' => 'Available'
        ]);

        if($request->exceeded_rental){
            $payment = Payment::create([
                'invoice' => $this->invoice($request),
                'rental_id' => $request->id,
                'total_payment' => $request->rental_info['total_payment'],
                'change' => $request->amount_tendered - $request->rental_info['total_payment'],
                'amount_tendered' => $request->amount_tendered
            ]);
    
        }

        return $this->success('Rental has been marked as finished successfully!', $rental);
    }

    public function store(Request $request){

        $car = Car::where('id', $request->car_id)->first();

        if($car->user_id == auth()->user()->id){
            return $this->error('You cannot rent your own car');
        }

        $rental = Rental::with(['rental_info'])->where(function($query){
            $query->where('status', 'Pending')
            ->orWhere('status', 'On-going');
        })->whereHas('rental_info', function($query){
            $query->whereBetween('pickup_date', [request('pickup_date'), request('return_date')])
            ->orWhereBetween('return_date', [request('pickup_date'), request('return_date')]);
        })->where('car_id', $request->car_id)->first();
        // return $this->error('', $rental);
        if(!empty($rental)){
            return $this->error('This car is not available for rent. ETA: ' . $rental->rental_info->return_date);
        }

        $response = '';
        if($request->payment_type == 'Credit Card'){
            try {
                $stripe = new \Stripe\StripeClient('sk_test_51KAQ3ADoJANsLvgsci2nG46EFVHLQbFxS6qfZ8UEpGl1ffYIVcDBsHOOKu7SVI9MBTCjBm43dSu8oKhQSSsK1uaF00ivv1r0U3');

                $token = $stripe->tokens->create([
                    'card' => [
                        'number' => $request->number,
                        'exp_month' => $request->exp_month,
                        'exp_year' => $request->exp_year,
                        'cvc' => $request->cvc,
                    ],
                ]);
        
                $stripecharge = new \Stripe\StripeClient('sk_test_51KAQ3ADoJANsLvgsci2nG46EFVHLQbFxS6qfZ8UEpGl1ffYIVcDBsHOOKu7SVI9MBTCjBm43dSu8oKhQSSsK1uaF00ivv1r0U3');
                $response = $stripecharge->charges->create([
                        "amount" => ($request->total + 5000)  * 100,
                        "currency" => "php",
                        "source" => $token,
                        "description" => "Renta Car payment for rental" 
                ]);
                
            }catch(Exception $e){
                return $this->error('Something went wrong!', $e->getMessage());
            }

            $data = [
                'pickup_date' => $request->pickup_date,
                'return_date' => $request->return_date,
                'additional_instruction' => $request->additional_instruction,
                'payment_type' => $request->payment_type,
                'payment_status' => 'Paid',
                'with_driver' => $request->with_driver,
                'driver_id' => $request->driver_id,
                'driver_payment' => $request->driver_payment,
                'drivers_license' => $request->drivers_license,
                'total_days' => $request->totalDays,
                'total_payment' => (($request->total - $request->driver_payment) * .12)  + $request->total + 5000,
            ];

            if($request->with_driver){
                $data['driver_id'] = $request->driver_id;
                $data['driver_payment'] = $request->driver_payment;
                $data['days_with_driver'] = $request->days_with_driver;
            }        

            $rentalInfo = RentalInfo::create($data);

            $rental = Rental::create([
                'transaction_number' => auth()->user()->id,
                'car_id' => $request->car_id,
                'rentee_id' => auth()->user()->id,
                'rental_info_id' => $rentalInfo->id,
                'invoice' => $response->receipt_url,
                'status' => 'On-going'
            ]);

            $rental->load(['user', 'rental_info']);

            $car = Car::where('id', $request->car_id)->first();
            if($car){
                $car->update([
                    'rental_status' => 'Rented'
                ]);
            }

            return response()->json(['msg' => 'Payment for rental is successful! You may check your account for the status', 'response' => $response]);
        }
        else {
            $data = [
                'pickup_date' => $request->pickup_date,
                'return_date' => $request->return_date,
                'payment_type' => $request->payment_type,
                'additional_instruction' => $request->additional_instruction,
                'payment_status' => 'Pending',
                'with_driver' => $request->with_driver,
                'drivers_license' => $request->drivers_license,
                'total_days' => $request->totalDays,
                'total_payment' => (($request->total - $request->driver_payment) * .12)  + $request->total + 5000
            ];

            if($request->with_driver){
                $data['driver_id'] = $request->driver_id;
                $data['driver_payment'] = $request->driver_payment;
                $data['days_with_driver'] = $request->days_with_driver;
            }      
            
            $rentalInfo = RentalInfo::create($data);

            $rental = Rental::create([
                'transaction_number' => auth()->user()->id,
                'car_id' => $request->car_id,
                'rentee_id' => auth()->user()->id,
                'rental_info_id' => $rentalInfo->id,
                'invoice' => $this->rentalInvoice($request),
                'status' => 'Pending'
            ]);

            $rental->load(['user', 'rental_info', 'car']);

            
            $car = Car::where('id', $request->car_id)->first();
            if($car){
                $car->update([
                    'rental_status' => 'Reserved'
                ]);
            }
            return $this->success('You can proceed to your account to check for your rental', $rental);
        }
    }

    public function rentalInvoice($data){
        $car = Car::with(['brand','user','user.info'])->where('id', $data->car_id)->first();

        $client = new Party([
            'name'  => $car->user->info->last_name . ', ' . $car->user->info->first_name . ' ' . $car->user->info->middle_name,
            'phone' => $car->user->info->contact_number,
            'custom_fields' => [
                'email' => $car->user->email,
                'address' => $car->user->info->address,
            ],
        ]);
        
        $customer = new Buyer([
            'name'          => auth()->user()->info->last_name . ', ' .auth()->user()->info->first_name . ' ' . auth()->user()->info->middle_name,
            'custom_fields' => [
                'email' => auth()->user()->email,
                'contact' => auth()->user()->info->contact_number,
                'address' => auth()->user()->info->address
            ],
        ]);

        $item = (new InvoiceItem())
        ->title($car->brand->brand . ' ' . $car->model)
        ->quantity($data->totalDays)
        ->taxByPercent(12)
        ->pricePerUnit(($data->total - $data->driver_payment) / $data->totalDays);

        $securityDeposit = (new InvoiceItem())
        ->title('Security Deposit')
        ->subTotalPrice(5000)
        ->pricePerUnit(5000);
        
        $driver = '';
        if($data->with_driver){
            $driver = (new InvoiceItem())
            ->title('Rental with driver')
            ->description('Days with driver')
            ->quantity($data->days_with_driver)
            ->pricePerUnit($data->driver_payment / $data->days_with_driver);
        }

        if($data->with_driver){
            $invoice = Invoice::make()
                ->buyer($customer)
                ->seller($client)
                ->currencySymbol('₱')
                ->currencyCode('PHP')
                ->currencyFormat('{SYMBOL}{VALUE}')
                ->addItem($item)
                ->addItem($driver)
                ->addItem($securityDeposit)
                ->logo(public_path('logo/logo.png'))
                ->filename('Invoice-'.time())
                ->save('invoice');
        }
        else {
            $invoice = Invoice::make()
                ->buyer($customer)
                ->seller($client)
                ->currencySymbol('₱')
                ->currencyCode('PHP')
                ->currencyFormat('{SYMBOL}{VALUE}')
                ->addItem($item)
                ->logo(public_path('logo/logo.png'))
                ->filename('Invoice-'.time())
                ->save('invoice');
        }

        $link = $invoice->url();
        return $link;
    }

    
    public function invoice($data){
        $car = Car::with(['brand','user','user.info'])->where('id', $data->car_id)->first();
        $rental = Rental::where('id', $data->id)->first();
        $user = User::with(['info'])->where('id', $rental->rentee_id)->first();
        $rental->load(['rental_info', 'car', 'user.info']);
        $client = new Party([
            'name'  => $car->user->info->last_name . ', ' . $car->user->info->first_name . ' ' . $car->user->info->middle_name,
            'phone' => $car->user->info->contact_number,
            'custom_fields' => [
                'email' => $car->user->email,
                'address' => $car->user->info->address,
            ],
        ]);
        
        $customer = new Buyer([
            'name'          => $user->info->last_name . ', ' .$user->info->first_name . ' ' . $user->info->middle_name,
            'custom_fields' => [
                'email' => $user->email,
                'contact' => $user->info->contact_number,
                'address' => $user->info->address
            ],
        ]);

        $itemPayment = (new InvoiceItem())
        ->title('Exceeding Payment')
        ->description('Additional payment for the exceeding days of rental before the car gets returned')
        ->quantity($data->total_days)
        ->subTotalPrice($data->total)
        ->pricePerUnit($data->total);

        $itemAmount = (new InvoiceItem())
        ->title('Amount Tendered')
        ->quantity(1)
        ->subTotalPrice($data->amount_tendered)
        ->pricePerUnit($data->amount_tendered);

        $itemChange = (new InvoiceItem())
        ->title('Change')
        ->quantity(1)
        ->pricePerUnit($data->amount_tendered - $data->total);
  
        $invoice = Invoice::make()
            ->buyer($customer)
            ->seller($client)
            ->currencySymbol('₱')
            ->currencyCode('PHP')
            ->template('invoice-paid')
            ->status(__('invoices::invoice.paid'))
            ->currencyFormat('{SYMBOL}{VALUE}')
            ->addItem($itemAmount)
            ->addItem($itemPayment)
            ->addItem($itemChange)
            ->logo(public_path('logo/logo.png'))
            ->filename('paid-exceeding-invoice-'.time())
            ->totalAmount($data->total)
            ->save('invoice');


        $link = $invoice->url();
        return $link;
    }

    public function destroy($id){
        Rental::destroy($id);
        $rental = Rental::onlyTrashed()->with(['car', 'car.brand', 'car.branch', 'user', 'user.info', 'rental_info'])->where('id', $id)->first();
        $rental->update([
            'status' => 'Cancelled'
        ]);
        $rental->car->update([
            'rental_status' => 'Available'
        ]);
        return $this->success('Rental transaction has been archived', $rental);
    }
}
